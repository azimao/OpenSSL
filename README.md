### 安全加密C语言库OpenSSL，在Android中服务器和客户端之间的签名验证和数据加密通信等。

### OpenSSL系列文章：
##### 一、[Android CMake轻松实现基于OpenSSL的HmacSHA1签名](http://www.jianshu.com/p/07df7626b4ee)
##### 二、[Android CMake轻松实现基于OpenSSL的SHA(1-512)签名](http://www.jianshu.com/p/4804f176daaf)
##### 三、[Android CMake轻松实现基于OpenSSL的MD5信息摘要&异或加解密](http://www.jianshu.com/p/768d8be14b93)
##### 四、[Android CMake轻松实现基于OpenSSL的AES加解密](http://www.jianshu.com/p/3f09a048a2cc)
##### 五、[Android CMake轻松实现基于OpenSSL的RSA加解密](http://www.jianshu.com/p/04eba47f7c07)
##### 六、[Android CMake轻松实现基于OpenSSL的RSA签名和验证](http://www.jianshu.com/p/18f1380e7922)
##### 七、[在Retrofit的基础上结合OpenSSL实现服务器和客户端之间数据加密通信](http://www.jianshu.com/p/970a8331143f)

### License
```
Copyright 2017 Phoenix, Inc.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```

备份
[Android 在 NDK 层使用 OpenSSL 进行 RSA 加密](https://fucknmb.com/2017/04/09/Android%E5%9C%A8NDK%E5%B1%82%E4%BD%BF%E7%94%A8OpenSSL%E8%BF%9B%E8%A1%8CRSA%E5%8A%A0%E5%AF%86/)  
[OpenSSL实践-Android下的编译和使用](https://www.jianshu.com/p/b790c548e787)